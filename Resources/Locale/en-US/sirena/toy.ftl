ent-PlushieRouter = router plushie
    .desc = A very peculiar stuffed toy resembling a distribution machine. It even has zip-up randomized bluespace portals!
    .suffix = { "" }

ent-PlushAurora = plush Aurora
    .desc = A nice touch plush toy made by Cybersun, which looks like one of the leaders of the fictional group Gorlaks. Sometimes it seems as if it moves.
    .suffix = { "" }